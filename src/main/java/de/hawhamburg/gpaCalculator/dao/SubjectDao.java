/**
 * SubjectDao interface
 *
 * @author mhelghamrawy
 * @version 1.0
 * @since 10.11.21
 */

package de.hawhamburg.gpaCalculator.dao;

import de.hawhamburg.gpaCalculator.domain.Subject;

import java.util.List;

public interface SubjectDao {
    List<Subject> selectAllSubjectsByStudyProgram(String program);
    List<Subject> selectSemesterSubjectsByStudyProgram(String program, int semester);
}
