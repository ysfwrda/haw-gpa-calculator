/**
 * SubjectDaoImplementation class implements
 *
 * @author mhelghamrawy
 * @version 1.0
 * @since 10.11.21
 */

package de.hawhamburg.gpaCalculator.dao.impl;

import de.hawhamburg.gpaCalculator.dao.SubjectDao;
import de.hawhamburg.gpaCalculator.domain.Subject;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public  class SubjectDaoImpl implements SubjectDao {
    @Override
    public List<Subject> selectAllSubjectsByStudyProgram(String program) {
        List<Subject> allSubjects = new ArrayList<>();

        allSubjects.add(new Subject("MA1", "Mathematics 1", 1, 8, 8));
        allSubjects.add(new Subject("MA2", "Mathematics 2", 2, 8, 8));
        allSubjects.add(new Subject("EE1", "Electrical Engineering 1", 1, 6, 6));
        allSubjects.add(new Subject("EE2", "Electrical Engineering 2", 2, 6, 6));
        allSubjects.add(new Subject("EL1", "Electronics 1", 2, 6, 6));

        allSubjects.add(new Subject("SO1", "Software Construction 1", 1, 7, 7));
        allSubjects.add(new Subject("SO2", "Software Construction 2", 2, 6, 6));
        allSubjects.add(new Subject("GE", "German", 1,4 , 0));
        allSubjects.add(new Subject("IC", "Intercultural Competence", 2, 3, 0));
        allSubjects.add(new Subject("LSL1", "Learning and Study Methods 1", 1, 4, 0));
        allSubjects.add(new Subject("LSL2", "Learning and Study Methods 2", 2, 2, 0));

        allSubjects.add(new Subject("SS1", "Signals and Systems 1", 3, 6, 12));
        allSubjects.add(new Subject("SS2", "Signals and Systems 2", 4, 6, 12));
        allSubjects.add(new Subject("EL2", "Electronics 2", 3, 7, 14));
        allSubjects.add(new Subject("DI", "Digital Circuits", 3, 6, 12));
        allSubjects.add(new Subject("DS", "Digital Systems", 4, 6, 12));

        allSubjects.add(new Subject("MC", "Microcontrollers", 4, 7, 14));
        allSubjects.add(new Subject("AD", "Algorithms and Data Structures", 3, 6, 12));
        allSubjects.add(new Subject("SE", "Software Engineering", 4, 6, 12));
        allSubjects.add(new Subject("DB", "Databases", 4, 6, 12));
        allSubjects.add(new Subject("EM", "Economics and Management", 3, 6, 12));

        allSubjects.add(new Subject("SP", "Scientific Writing", 5, 4, 9));
        allSubjects.add(new Subject("IP", "Internship", 5, 25, 0));
        allSubjects.add(new Subject("BU", "Bus Systems", 6, 6, 12));
        allSubjects.add(new Subject("OS", "Operating Systems", 6, 6, 12));
        allSubjects.add(new Subject("DP", "Digital Signal Processing", 6, 6, 12));

        allSubjects.add(new Subject("DC", "Digital Communication Systems", 6, 6, 12));
        allSubjects.add(new Subject("CJ1", "Elective Project", 6, 5, 0));
        allSubjects.add(new Subject("CJ2", "Compulsory Project", 7, 5, 0));
        allSubjects.add(new Subject("CM1", "Elective Course 1", 7, 5, 10));
        allSubjects.add(new Subject("CM2", "Elective Course 2", 7,5, 10));
        allSubjects.add(new Subject("BT", "Bachelor Thesis", 7, 15, 70));

        return allSubjects;
    }

    @Override
    public List<Subject> selectSemesterSubjectsByStudyProgram(String program, int semester) {
        List<Subject> allSubjects = selectAllSubjectsByStudyProgram("ie");

        List<Subject> semesterSubjects = allSubjects.stream()
                .filter(s -> s.getSemester() == semester)
                .collect(Collectors.toList());

        return semesterSubjects;
    }
}
