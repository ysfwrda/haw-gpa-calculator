/**
 * ControllerTemplate class implements
 *
 * @author mhelghamrawy
 * @version 1.0
 * @since 30.12.21
 */

package de.hawhamburg.gpaCalculator.util;

import java.util.Map;
import java.util.stream.Collectors;

public abstract class ControllerTemplate {

    protected static Map<String, Integer> convertToIntMap(Map<String, String> input) {
        return input.entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> Integer.parseInt(e.getValue())));
    }
}
