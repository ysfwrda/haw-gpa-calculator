/**
 * HomeController class implements
 *
 * @author mhelghamrawy
 * @version 1.0
 * @since 07.11.21
 */

package de.hawhamburg.gpaCalculator.guest;

import de.hawhamburg.gpaCalculator.domain.Transcript;
import de.hawhamburg.gpaCalculator.model.TranscriptBuilder;

import de.hawhamburg.gpaCalculator.util.ControllerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/guest/transcript", consumes = {}, produces = MediaType.APPLICATION_JSON_VALUE)
public class GuestController extends ControllerTemplate {
    private TranscriptBuilder transcriptBuilder;

    @Autowired
    public GuestController(TranscriptBuilder transcriptBuilder) {
        this.transcriptBuilder = transcriptBuilder;
    }

    @GetMapping(value = "")
    Transcript calculateTranscript(@RequestParam Map<String, String> subjectGrades) {
        return transcriptBuilder.build(convertToIntMap(subjectGrades));
    }

    @GetMapping(value = "/gpa", produces = MediaType.APPLICATION_JSON_VALUE)
    Transcript calculateOverallGpa(@RequestParam Map<String, String> subjectGrades) {
        Transcript studentTranscript = transcriptBuilder.calculate(convertToIntMap(subjectGrades));
        return studentTranscript;
    }

    @GetMapping(value = "/gpa/{semester_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    Transcript calculateSemesterGpa(
            @RequestParam Map<String, String> subjectGrades,
            @PathVariable("semester_id") int semesterId) {
        return transcriptBuilder.calculate(convertToIntMap(subjectGrades), semesterId);
    }
}
