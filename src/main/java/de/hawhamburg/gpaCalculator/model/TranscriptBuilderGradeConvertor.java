/**
 * initHawToUsGradeMapHashMapInitializer class implements
 *
 * @author mhelghamrawy
 * @version 1.0
 * @since 30.12.21
 */

package de.hawhamburg.gpaCalculator.model;

import java.util.HashMap;

public class TranscriptBuilderGradeConvertor {
    static HashMap<Integer, Double> initHawToDeGradeMapHashMap() {
        HashMap<Integer, Double> HAW_TO_DE_GRADE_MAP = new HashMap<>();

        HAW_TO_DE_GRADE_MAP.put(15, 0.7);
        HAW_TO_DE_GRADE_MAP.put(14, 1.0);
        HAW_TO_DE_GRADE_MAP.put(13, 1.3);
        HAW_TO_DE_GRADE_MAP.put(12, 1.7);
        HAW_TO_DE_GRADE_MAP.put(11, 2.0);

        HAW_TO_DE_GRADE_MAP.put(10, 2.3);
        HAW_TO_DE_GRADE_MAP.put(9, 2.7);
        HAW_TO_DE_GRADE_MAP.put(8, 3.0);
        HAW_TO_DE_GRADE_MAP.put(7, 3.3);
        HAW_TO_DE_GRADE_MAP.put(6, 3.7);

        HAW_TO_DE_GRADE_MAP.put(5, 4.0);

        return HAW_TO_DE_GRADE_MAP;
    }
    static HashMap<Integer, Double> initHawToUsGradeMapHashMap() {
        HashMap<Integer, Double> HAW_TO_US_GRADE_MAP = new HashMap<>();

        HAW_TO_US_GRADE_MAP.put(15, 4.4);
        HAW_TO_US_GRADE_MAP.put(14, 4.0);
        HAW_TO_US_GRADE_MAP.put(13, 3.7);
        HAW_TO_US_GRADE_MAP.put(12, 3.3);
        HAW_TO_US_GRADE_MAP.put(11, 3.0);

        HAW_TO_US_GRADE_MAP.put(10, 2.7);
        HAW_TO_US_GRADE_MAP.put(9, 2.3);
        HAW_TO_US_GRADE_MAP.put(8, 2.0);
        HAW_TO_US_GRADE_MAP.put(7, 1.7);
        HAW_TO_US_GRADE_MAP.put(6, 1.3);

        HAW_TO_US_GRADE_MAP.put(5, 1.0);

        return HAW_TO_US_GRADE_MAP;
    }
}
