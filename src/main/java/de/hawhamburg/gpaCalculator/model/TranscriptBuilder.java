/**
 * GradeCalculatorService class implements
 *
 * @author mhelghamrawy
 * @version 1.0
 * @since 07.11.21
 */

package de.hawhamburg.gpaCalculator.model;

import de.hawhamburg.gpaCalculator.domain.Subject;
import de.hawhamburg.gpaCalculator.dao.SubjectDao;
import de.hawhamburg.gpaCalculator.domain.Transcript;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import static de.hawhamburg.gpaCalculator.model.TranscriptBuilderGradeConvertor.*;


@NoArgsConstructor
@Service
public class TranscriptBuilder {
    private SubjectDao subjectDao;

    private static final HashMap<Integer,Double> HAW_TO_DE_GRADE_MAP = initHawToDeGradeMapHashMap();
    private static final HashMap<Integer,Double> HAW_TO_US_GRADE_MAP = initHawToUsGradeMapHashMap();

    @Autowired
    public TranscriptBuilder(SubjectDao subjectDao) {
        this.subjectDao = subjectDao;
    }

    public Transcript build(Map<String, Integer> subjectGrades) {
        final Transcript studentTranscript = calculate(subjectGrades);

        final List<Transcript> semesterTranscripts = new ArrayList<>();

        for (int semester = 1; semester <= 7; semester++) {
            Transcript semesterTranscript = calculate(subjectGrades, semester);

            semesterTranscripts.add(semesterTranscript);
        }

        studentTranscript.setSubjects(null);

        studentTranscript.setSemesterTranscripts(semesterTranscripts);

        return studentTranscript;
    }

    public Transcript calculate(Map<String, Integer> subjectGrades) {
        final Transcript studentTranscript = new Transcript();
        studentTranscript.setSubjects(subjectDao.selectAllSubjectsByStudyProgram("ie"));

        return calculate(studentTranscript, subjectGrades);
    }
    public Transcript calculate(Map<String, Integer> subjectGrades, int semester) {
        final Transcript studentTranscript = new Transcript();
        studentTranscript.setSubjects(subjectDao.selectSemesterSubjectsByStudyProgram("ie", semester));

        return calculate(studentTranscript, subjectGrades);
    }

    private Transcript calculate(Transcript studentTranscript, Map<String, Integer> subjectGrades) {
        int completedCreditPoints = 0;
        int weightingsSum = 0;

        int weightedSumHaw = 0;
        double weightedSumDe = 0.0;
        double weightedSumUs = 0.0;

        for(String subject : subjectGrades.keySet()) {
            // find corresponding element in list of subjects
        	Object[] filteredSubjects = studentTranscript
                    .getSubjects()
                    .stream()
                    .filter(s -> s.getAbbreviation().equals(subject))
                    .toArray();
        	
        	if(filteredSubjects.length > 0){
	            Subject subjectInTranscript = (Subject) filteredSubjects[0];
	
	            if(subjectGrades.get(subject) == 373) {
                    // fill haw grade from gradesInput
                    subjectInTranscript.setGradeHaw(subjectGrades.get(subject));
                    // sum credit points
                    completedCreditPoints += subjectInTranscript.getCreditPoints();
                }
                else if(subjectGrades.get(subject) >= 5) {
	                // fill haw grade from gradesInput
	                subjectInTranscript.setGradeHaw(subjectGrades.get(subject));
	                // sum credit points
	                completedCreditPoints += subjectInTranscript.getCreditPoints();
	                // sum weightings
	                weightingsSum += subjectInTranscript.getWeighting();
	                // calculate the weighted sum for HAW grades
	                weightedSumHaw += subjectGrades.get(subject) * subjectInTranscript.getWeighting();
	                // convert and calculate the weighted sum for DE grades
	                weightedSumDe += HAW_TO_DE_GRADE_MAP.get(subjectGrades.get(subject)) * subjectInTranscript.getWeighting();
	                // convert and calculate the weighted sum for US grades
	                weightedSumUs += HAW_TO_US_GRADE_MAP.get(subjectGrades.get(subject)) * subjectInTranscript.getWeighting();
	            }
        	}
        }

        studentTranscript.setRegularCreditPoints(calculateRegularCreditPoints(studentTranscript));
        studentTranscript.setCompletedCreditPoints(completedCreditPoints);
        studentTranscript.setRegularWeightedCreditPoints(completedRegularWeightedCreditPoints(studentTranscript));
        studentTranscript.setCompletedWeightedCreditPoints(weightedSumHaw);
        studentTranscript.setGpaHaw((double) weightedSumHaw / weightingsSum);
        studentTranscript.setGpaDe(weightedSumDe / weightingsSum);
        studentTranscript.setGpaUs(weightedSumUs / weightingsSum);

        return studentTranscript;
    }

    private int calculateRegularCreditPoints(Transcript transcript) {
        return transcript.getSubjects().stream().mapToInt(Subject::getCreditPoints).sum();
    }

    private int completedRegularWeightedCreditPoints(Transcript transcript) {
        return transcript.getSubjects().stream().mapToInt(Subject::getWeighting).sum();
    }
}