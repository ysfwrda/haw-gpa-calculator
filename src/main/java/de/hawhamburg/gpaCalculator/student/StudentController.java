/**
 * HomeController class implements
 *
 * @author mhelghamrawy
 * @version 1.0
 * @since 07.11.21
 */

package de.hawhamburg.gpaCalculator.student;

import de.hawhamburg.gpaCalculator.domain.Transcript;
import de.hawhamburg.gpaCalculator.model.TranscriptBuilder;
import de.hawhamburg.gpaCalculator.util.ControllerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/student/{student_id}/transcript")
public class StudentController extends ControllerTemplate {
    private TranscriptBuilder transcriptBuilder;

    @Autowired
    public StudentController(TranscriptBuilder transcriptBuilder) {
        this.transcriptBuilder = transcriptBuilder;
    }
}
