/**
 * Transcript class implements
 *
 * @author mhelghamrawy
 * @version 1.0
 * @since 07.11.21
 */

package de.hawhamburg.gpaCalculator.domain;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.io.IOException;
import java.util.List;

@Data
@NoArgsConstructor
@JsonAutoDetect(
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonPropertyOrder({
        "subjects",
        "semester_regular_cp",
        "semester_completed_cp",
        "semester_regular_weighted_cp",
        "semester_completed_weighted_cp",
        "gpa_haw",
        "gpa_de",
        "gpa_us",
        "semesters"})
public class Transcript {
    @JsonProperty("subjects")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<Subject> subjects;

    @NonNull
    @JsonProperty("semester_regular_cp")
    private int regularCreditPoints;

    @NonNull
    @JsonProperty("semester_completed_cp")
    private int completedCreditPoints;

    @NonNull
    @JsonProperty("semester_regular_weighted_cp")
    private int regularWeightedCreditPoints;

    @NonNull
    @JsonProperty("semester_completed_weighted_cp")
    private int completedWeightedCreditPoints;

    @NonNull
    @JsonProperty("gpa_haw")
    @JsonSerialize(using = DecimalJsonSerializer.class)
    private double gpaHaw;

    @NonNull
    @JsonProperty("gpa_de")
    @JsonSerialize(using = DecimalJsonSerializer.class)
    private double gpaDe;

    @NonNull
    @JsonProperty("gpa_us")
    @JsonSerialize(using = DecimalJsonSerializer.class)
    private double gpaUs;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("semesters")
    private List<Transcript> semesterTranscripts;
}

class DecimalJsonSerializer extends JsonSerializer<Double> {
    @Override
    public void serialize(Double value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException {
        if(!Double.isNaN(value)) {
            jgen.writeNumber(String.format("%.2f", value));
        }
        else {
            jgen.writeNumber(String.format("%.2f", 0.0));
        }
    }
}

