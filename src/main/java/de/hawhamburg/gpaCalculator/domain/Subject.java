/**
 * Subject class implements
 *
 * @author mhelghamrawy
 * @version 1.0
 * @since 07.11.21
 */

package de.hawhamburg.gpaCalculator.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NonNull;

@Data
public class Subject {

    @NonNull
    String abbreviation;
    @NonNull
    String name;
    @NonNull
    int semester;
    @NonNull
    int creditPoints;
    @NonNull
    int weighting;

    @JsonIgnore
    double gradeHaw;
    @JsonIgnore
    double gradeDe;
    @JsonIgnore
    double gradeUs;

    @Override
    public String toString() {
        return "Subject{" +
                "abbreviation='" + abbreviation + '\'' +
                ", name='" + name + '\'' +
                ", semester=" + semester +
                ", creditPoints=" + creditPoints +
                ", weighting=" + weighting +
                ", gradeHaw=" + gradeHaw +
                ", gradeDe=" + gradeDe +
                ", gradeUs=" + gradeUs +
                '}';
    }
}
