/**
 * SubjectsRetrievalService class implements
 *
 * @author mhelghamrawy
 * @version 1.0
 * @since 14.12.21
 */

package de.hawhamburg.gpaCalculator.subjects;

import de.hawhamburg.gpaCalculator.dao.SubjectDao;
import de.hawhamburg.gpaCalculator.domain.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SubjectsRetriever {
    @Autowired
    SubjectDao subjectDao;

    public List<Subject> getSubjects() {
        return subjectDao.selectAllSubjectsByStudyProgram("ie");
    }

    public List<Subject> getSubjectsBySemester(int semester) {
        return subjectDao.selectSemesterSubjectsByStudyProgram("ie", semester);
    }

    public List<String> getSubjectAbbreviations() {
        return subjectDao.selectAllSubjectsByStudyProgram("ie")
                .stream()
                .map(Subject::getAbbreviation)
                .collect(Collectors.toList());
    }

    public List<String> getSubjectAbbreviationsBySemester(int semester) {
        return subjectDao.selectSemesterSubjectsByStudyProgram("ie", semester)
                .stream()
                .filter(s -> s.getSemester() == semester)
                .map(Subject::getAbbreviation)
                .collect(Collectors.toList());
    }
}
