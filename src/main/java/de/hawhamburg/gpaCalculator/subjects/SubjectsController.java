/**
 * TranscriptSubjectsController class implements
 *
 * @author mhelghamrawy
 * @version 1.0
 * @since 14.12.21
 */

package de.hawhamburg.gpaCalculator.subjects;

import de.hawhamburg.gpaCalculator.domain.Subject;
import de.hawhamburg.gpaCalculator.util.ControllerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/subjects", consumes = {}, produces = MediaType.APPLICATION_JSON_VALUE)
public class SubjectsController extends ControllerTemplate {
    @Autowired
    private SubjectsRetriever subjectsRetriever;

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Subject> getSubjects() {
        return subjectsRetriever.getSubjects();
    }

    @GetMapping(value = "/{semester_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Subject> getSubjects(@PathVariable("semester_id") int semesterId) {
        return subjectsRetriever.getSubjectsBySemester(semesterId);
    }

    @GetMapping(value = "/abbreviations", produces = MediaType.APPLICATION_JSON_VALUE)
    List<String> getSubjectAbbreviations() {
        return subjectsRetriever.getSubjectAbbreviations();
    }

    @GetMapping(value = "/{semester_id}/abbreviations", produces = MediaType.APPLICATION_JSON_VALUE)
    List<String> getSubjectAbbreviations(@PathVariable("semester_id") int semesterId) {
        return subjectsRetriever.getSubjectAbbreviationsBySemester(semesterId);
    }
}
