package de.hawhamburg.gpaCalculator.model;

import de.hawhamburg.gpaCalculator.domain.Transcript;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class TranscriptBuilderTest {
    private static TranscriptBuilder transcriptBuilder;
    private static Map<String, Integer> grades20210921 = new HashMap<>();

    @Autowired
    public TranscriptBuilderTest(TranscriptBuilder transcriptBuilder) {
        TranscriptBuilderTest.transcriptBuilder = transcriptBuilder;
    }

    @BeforeAll
    static void initGrades20210921() {
        grades20210921.put("EE1", 7);
        grades20210921.put("SO1", 11);
        grades20210921.put("GE", 373);
        grades20210921.put("LSL1", 373);

        grades20210921.put("MA2", 8);
        grades20210921.put("SO2", 14);
        grades20210921.put("IC", 373);
        grades20210921.put("LSL2", 373);

        grades20210921.put("SS1", 12);
        grades20210921.put("EL2", 14);
        grades20210921.put("DI", 6);
        grades20210921.put("DS", 14);
        grades20210921.put("AD", 14);
        grades20210921.put("DB", 13);
        grades20210921.put("EM", 15);
    }

    @Test
    void build_ie1_Cp() {
        Transcript testTranscript = transcriptBuilder.build(new HashMap<>());
        List<Transcript> semesterTranscripts = testTranscript.getSemesterTranscripts();

        assertEquals(29, semesterTranscripts.get(0).getRegularCreditPoints(), "IE semester 1 Regular CPs were calculated unsuccessfully");
    }

    @Test
    void build_ie2_Cp() {
        Transcript testTranscript = transcriptBuilder.build(new HashMap<>());
        List<Transcript> semesterTranscripts = testTranscript.getSemesterTranscripts();

        assertEquals(31, semesterTranscripts.get(1).getRegularCreditPoints(), "IE semester 2 Regular CPs were calculated unsuccessfully");
    }

    @Test
    void build_ie3_Cp() {
        Transcript testTranscript = transcriptBuilder.build(new HashMap<>());
        List<Transcript> semesterTranscripts = testTranscript.getSemesterTranscripts();

        assertEquals(31, semesterTranscripts.get(2).getRegularCreditPoints(), "IE semester 3 Regular CPs were calculated unsuccessfully");
    }

    @Test
    void build_ie4_Cp() {
        Transcript testTranscript = transcriptBuilder.build(new HashMap<>());
        List<Transcript> semesterTranscripts = testTranscript.getSemesterTranscripts();

        assertEquals(31, semesterTranscripts.get(3).getRegularCreditPoints(), "IE semester 4 Regular CPs were calculated unsuccessfully");
    }

    @Test
    void build_ie5_Cp() {
        Transcript testTranscript = transcriptBuilder.build(new HashMap<>());
        List<Transcript> semesterTranscripts = testTranscript.getSemesterTranscripts();

        assertEquals(29, semesterTranscripts.get(4).getRegularCreditPoints(), "IE semester 5 Regular CPs were calculated unsuccessfully");
    }

    @Test
    void build_ie6_Cp() {
        Transcript testTranscript = transcriptBuilder.build(new HashMap<>());
        List<Transcript> semesterTranscripts = testTranscript.getSemesterTranscripts();

        assertEquals(29, semesterTranscripts.get(5).getRegularCreditPoints(), "IE semester 6 Regular CPs were calculated unsuccessfully");
    }

    @Test
    void build_ie7_Cp() {
        Transcript testTranscript = transcriptBuilder.build(new HashMap<>());
        List<Transcript> semesterTranscripts = testTranscript.getSemesterTranscripts();

        assertEquals(30, semesterTranscripts.get(6).getRegularCreditPoints(), "IE semester 7 Regular CPs were calculated unsuccessfully");
    }

    @Test
    void calculate_ie_Cp() {
        Transcript testTranscript = transcriptBuilder.build(new HashMap<>());

        assertEquals(210, testTranscript.getRegularCreditPoints(), "IE semester Regular CPs were calculated unsuccessfully");
    }

    @Test
    void build_20210921_GpaHaw() {
        final double TEST_TOLERANCE = 0.001;

        Transcript testTranscript = transcriptBuilder.build(grades20210921);

        assertEquals(11.96, testTranscript.getGpaHaw(), 11.96*TEST_TOLERANCE, "Mohamed's 20.09.2021 HAW GPA is calculated unsuccessfully");
    }

    @Test
    void build_20210921_GpaDe() {
        final double TEST_TOLERANCE = 0.02;

        Transcript testTranscript = transcriptBuilder.build(grades20210921);

        assertEquals(1.71, testTranscript.getGpaDe(), 1.71*TEST_TOLERANCE, "Mohamed's 20.09.2021 DE GPA is calculated unsuccessfully");
    }

    @Test
    void build_20210921_GpaUs() {
        final double TEST_TOLERANCE = 0.01;

        Transcript testTranscript = transcriptBuilder.build(grades20210921);

        assertEquals(3.32, testTranscript.getGpaUs(), 3.32*TEST_TOLERANCE, "Mohamed's 20.09.2021 US GPA is calculated unsuccessfully");
    }

    @Test
    void build_20210921_ie1_Cp() {
        Transcript testTranscript = transcriptBuilder.build(grades20210921);
        List<Transcript> semesterTranscripts = testTranscript.getSemesterTranscripts();

        assertEquals(21, semesterTranscripts.get(0).getCompletedCreditPoints(), "IE semester 1 Regular CPs were calculated unsuccessfully");
    }

    @Test
    void build_20210921_ie2_Cp() {
        Transcript testTranscript = transcriptBuilder.build(grades20210921);
        List<Transcript> semesterTranscripts = testTranscript.getSemesterTranscripts();

        assertEquals(19, semesterTranscripts.get(1).getCompletedCreditPoints(), "IE semester 1 Regular CPs were calculated unsuccessfully");
    }

    @Test
    void build_20210921_ie3_Cp() {
        Transcript testTranscript = transcriptBuilder.build(grades20210921);
        List<Transcript> semesterTranscripts = testTranscript.getSemesterTranscripts();

        assertEquals(31, semesterTranscripts.get(2).getCompletedCreditPoints(), "IE semester 1 Regular CPs were calculated unsuccessfully");
    }

    @Test
    void build_20210921_ie4_Cp() {
        Transcript testTranscript = transcriptBuilder.build(grades20210921);
        List<Transcript> semesterTranscripts = testTranscript.getSemesterTranscripts();

        assertEquals(12, semesterTranscripts.get(3).getCompletedCreditPoints(), "IE semester 1 Regular CPs were calculated unsuccessfully");
    }

    @Test
    void build_20210921_Cp() {
        Transcript testTranscript = transcriptBuilder.build(grades20210921);

        assertEquals(83, testTranscript.getCompletedCreditPoints(), "Mohamed's 20.09.2021 completed CPs are calculated unsuccessfully");
    }
}
