package de.hawhamburg.gpaCalculator.guest;

import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class GuestControllerTest {
    @Autowired
    private MockMvc mockMvc;
    static MultiValueMap <String, String>gradesMap;
    private
    @BeforeAll
    static void intializeGradesMap(){
        gradesMap = new LinkedMultiValueMap<>();
        gradesMap.add("MA1", "11");
        gradesMap.add("MA2", "11");
        gradesMap.add("EE1", "6");
        gradesMap.add("EE2", "14");
        gradesMap.add("EL1", "14");
        gradesMap.add("SO1", "14");
        gradesMap.add("SO2", "13");
        gradesMap.add("GE", "14");
        gradesMap.add("IC", "15");
        gradesMap.add("LSL1", "11");
        gradesMap.add("LSL2", "11");
        gradesMap.add("SS1", "12");
        gradesMap.add("SS2", "6");
        gradesMap.add("EL2", "15");
        gradesMap.add("DI", "12");
        gradesMap.add("AD", "13");
        gradesMap.add("SE", "13");
        gradesMap.add("DB", "12");
        gradesMap.add("EM", "13");
        gradesMap.add("SP", "15");
        gradesMap.add("BU", "15");
        gradesMap.add("OS", "6");
        gradesMap.add("DS", "10");
    }
    @Test
    void calculateTranscript_empty() throws Exception {
        var transcriptEmptyJSON = "{\"semester_regular_cp\":210,\"semester_completed_cp\":0,\"semester_regular_weighted_cp\":318,\"semester_completed_weighted_cp\":0,\"gpa_haw\":0.00,\"gpa_de\":0.00,\"gpa_us\":0.00,\"semesters\":[{\"subjects\":[{\"abbreviation\":\"MA1\",\"name\":\"Mathematics 1\",\"semester\":1,\"creditPoints\":8,\"weighting\":8},{\"abbreviation\":\"EE1\",\"name\":\"Electrical Engineering 1\",\"semester\":1,\"creditPoints\":6,\"weighting\":6},{\"abbreviation\":\"SO1\",\"name\":\"Software Construction 1\",\"semester\":1,\"creditPoints\":7,\"weighting\":7},{\"abbreviation\":\"GE\",\"name\":\"German\",\"semester\":1,\"creditPoints\":4,\"weighting\":0},{\"abbreviation\":\"LSL1\",\"name\":\"Learning and Study Methods 1\",\"semester\":1,\"creditPoints\":4,\"weighting\":0}],\"semester_regular_cp\":29,\"semester_completed_cp\":0,\"semester_regular_weighted_cp\":21,\"semester_completed_weighted_cp\":0,\"gpa_haw\":0.00,\"gpa_de\":0.00,\"gpa_us\":0.00},{\"subjects\":[{\"abbreviation\":\"MA2\",\"name\":\"Mathematics 2\",\"semester\":2,\"creditPoints\":8,\"weighting\":8},{\"abbreviation\":\"EE2\",\"name\":\"Electrical Engineering 2\",\"semester\":2,\"creditPoints\":6,\"weighting\":6},{\"abbreviation\":\"EL1\",\"name\":\"Electronics 1\",\"semester\":2,\"creditPoints\":6,\"weighting\":6},{\"abbreviation\":\"SO2\",\"name\":\"Software Construction 2\",\"semester\":2,\"creditPoints\":6,\"weighting\":6},{\"abbreviation\":\"IC\",\"name\":\"Intercultural Competence\",\"semester\":2,\"creditPoints\":3,\"weighting\":0},{\"abbreviation\":\"LSL2\",\"name\":\"Learning and Study Methods 2\",\"semester\":2,\"creditPoints\":2,\"weighting\":0}],\"semester_regular_cp\":31,\"semester_completed_cp\":0,\"semester_regular_weighted_cp\":26,\"semester_completed_weighted_cp\":0,\"gpa_haw\":0.00,\"gpa_de\":0.00,\"gpa_us\":0.00},{\"subjects\":[{\"abbreviation\":\"SS1\",\"name\":\"Signals and Systems 1\",\"semester\":3,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"EL2\",\"name\":\"Electronics 2\",\"semester\":3,\"creditPoints\":7,\"weighting\":14},{\"abbreviation\":\"DI\",\"name\":\"Digital Circuits\",\"semester\":3,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"AD\",\"name\":\"Algorithms and Data Structures\",\"semester\":3,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"EM\",\"name\":\"Economics and Management\",\"semester\":3,\"creditPoints\":6,\"weighting\":12}],\"semester_regular_cp\":31,\"semester_completed_cp\":0,\"semester_regular_weighted_cp\":62,\"semester_completed_weighted_cp\":0,\"gpa_haw\":0.00,\"gpa_de\":0.00,\"gpa_us\":0.00},{\"subjects\":[{\"abbreviation\":\"SS2\",\"name\":\"Signals and Systems 2\",\"semester\":4,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"DS\",\"name\":\"Digital Systems\",\"semester\":4,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"MC\",\"name\":\"Microcontrollers\",\"semester\":4,\"creditPoints\":7,\"weighting\":14},{\"abbreviation\":\"SE\",\"name\":\"Software Engineering\",\"semester\":4,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"DB\",\"name\":\"Databases\",\"semester\":4,\"creditPoints\":6,\"weighting\":12}],\"semester_regular_cp\":31,\"semester_completed_cp\":0,\"semester_regular_weighted_cp\":62,\"semester_completed_weighted_cp\":0,\"gpa_haw\":0.00,\"gpa_de\":0.00,\"gpa_us\":0.00},{\"subjects\":[{\"abbreviation\":\"SP\",\"name\":\"Scientific Writing\",\"semester\":5,\"creditPoints\":4,\"weighting\":9},{\"abbreviation\":\"IP\",\"name\":\"Internship\",\"semester\":5,\"creditPoints\":25,\"weighting\":0}],\"semester_regular_cp\":29,\"semester_completed_cp\":0,\"semester_regular_weighted_cp\":9,\"semester_completed_weighted_cp\":0,\"gpa_haw\":0.00,\"gpa_de\":0.00,\"gpa_us\":0.00},{\"subjects\":[{\"abbreviation\":\"BU\",\"name\":\"Bus Systems\",\"semester\":6,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"OS\",\"name\":\"Operating Systems\",\"semester\":6,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"DP\",\"name\":\"Digital Signal Processing\",\"semester\":6,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"DC\",\"name\":\"Digital Communication Systems\",\"semester\":6,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"CJ1\",\"name\":\"Elective Project\",\"semester\":6,\"creditPoints\":5,\"weighting\":0}],\"semester_regular_cp\":29,\"semester_completed_cp\":0,\"semester_regular_weighted_cp\":48,\"semester_completed_weighted_cp\":0,\"gpa_haw\":0.00,\"gpa_de\":0.00,\"gpa_us\":0.00},{\"subjects\":[{\"abbreviation\":\"CJ2\",\"name\":\"Compulsory Project\",\"semester\":7,\"creditPoints\":5,\"weighting\":0},{\"abbreviation\":\"CM1\",\"name\":\"Elective Course 1\",\"semester\":7,\"creditPoints\":5,\"weighting\":10},{\"abbreviation\":\"CM2\",\"name\":\"Elective Course 2\",\"semester\":7,\"creditPoints\":5,\"weighting\":10},{\"abbreviation\":\"BT\",\"name\":\"Bachelor Thesis\",\"semester\":7,\"creditPoints\":15,\"weighting\":70}],\"semester_regular_cp\":30,\"semester_completed_cp\":0,\"semester_regular_weighted_cp\":90,\"semester_completed_weighted_cp\":0,\"gpa_haw\":0.00,\"gpa_de\":0.00,\"gpa_us\":0.00}]}";
        this.mockMvc.perform(get("/guest/transcript")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().json(transcriptEmptyJSON));
    }

    @Test
    void calculateTranscript_9112021() throws Exception {
        var transcriptJSON = "{\"semester_regular_cp\":210,\"semester_completed_cp\":131,\"semester_regular_weighted_cp\":318,\"semester_completed_weighted_cp\":2245,\"gpa_haw\":11.82,\"gpa_de\":1.74,\"gpa_us\":3.28,\"semesters\":[{\"subjects\":[{\"abbreviation\":\"MA1\",\"name\":\"Mathematics 1\",\"semester\":1,\"creditPoints\":8,\"weighting\":8},{\"abbreviation\":\"EE1\",\"name\":\"Electrical Engineering 1\",\"semester\":1,\"creditPoints\":6,\"weighting\":6},{\"abbreviation\":\"SO1\",\"name\":\"Software Construction 1\",\"semester\":1,\"creditPoints\":7,\"weighting\":7},{\"abbreviation\":\"GE\",\"name\":\"German\",\"semester\":1,\"creditPoints\":4,\"weighting\":0},{\"abbreviation\":\"LSL1\",\"name\":\"Learning and Study Methods 1\",\"semester\":1,\"creditPoints\":4,\"weighting\":0}],\"semester_regular_cp\":29,\"semester_completed_cp\":29,\"semester_regular_weighted_cp\":21,\"semester_completed_weighted_cp\":222,\"gpa_haw\":10.57,\"gpa_de\":2.15,\"gpa_us\":2.85},{\"subjects\":[{\"abbreviation\":\"MA2\",\"name\":\"Mathematics 2\",\"semester\":2,\"creditPoints\":8,\"weighting\":8},{\"abbreviation\":\"EE2\",\"name\":\"Electrical Engineering 2\",\"semester\":2,\"creditPoints\":6,\"weighting\":6},{\"abbreviation\":\"EL1\",\"name\":\"Electronics 1\",\"semester\":2,\"creditPoints\":6,\"weighting\":6},{\"abbreviation\":\"SO2\",\"name\":\"Software Construction 2\",\"semester\":2,\"creditPoints\":6,\"weighting\":6},{\"abbreviation\":\"IC\",\"name\":\"Intercultural Competence\",\"semester\":2,\"creditPoints\":3,\"weighting\":0},{\"abbreviation\":\"LSL2\",\"name\":\"Learning and Study Methods 2\",\"semester\":2,\"creditPoints\":2,\"weighting\":0}],\"semester_regular_cp\":31,\"semester_completed_cp\":31,\"semester_regular_weighted_cp\":26,\"semester_completed_weighted_cp\":334,\"gpa_haw\":12.85,\"gpa_de\":1.38,\"gpa_us\":3.62},{\"subjects\":[{\"abbreviation\":\"SS1\",\"name\":\"Signals and Systems 1\",\"semester\":3,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"EL2\",\"name\":\"Electronics 2\",\"semester\":3,\"creditPoints\":7,\"weighting\":14},{\"abbreviation\":\"DI\",\"name\":\"Digital Circuits\",\"semester\":3,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"AD\",\"name\":\"Algorithms and Data Structures\",\"semester\":3,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"EM\",\"name\":\"Economics and Management\",\"semester\":3,\"creditPoints\":6,\"weighting\":12}],\"semester_regular_cp\":31,\"semester_completed_cp\":31,\"semester_regular_weighted_cp\":62,\"semester_completed_weighted_cp\":810,\"gpa_haw\":13.06,\"gpa_de\":1.32,\"gpa_us\":3.70},{\"subjects\":[{\"abbreviation\":\"SS2\",\"name\":\"Signals and Systems 2\",\"semester\":4,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"DS\",\"name\":\"Digital Systems\",\"semester\":4,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"MC\",\"name\":\"Microcontrollers\",\"semester\":4,\"creditPoints\":7,\"weighting\":14},{\"abbreviation\":\"SE\",\"name\":\"Software Engineering\",\"semester\":4,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"DB\",\"name\":\"Databases\",\"semester\":4,\"creditPoints\":6,\"weighting\":12}],\"semester_regular_cp\":31,\"semester_completed_cp\":24,\"semester_regular_weighted_cp\":62,\"semester_completed_weighted_cp\":492,\"gpa_haw\":10.25,\"gpa_de\":2.25,\"gpa_us\":2.75},{\"subjects\":[{\"abbreviation\":\"SP\",\"name\":\"Scientific Writing\",\"semester\":5,\"creditPoints\":4,\"weighting\":9},{\"abbreviation\":\"IP\",\"name\":\"Internship\",\"semester\":5,\"creditPoints\":25,\"weighting\":0}],\"semester_regular_cp\":29,\"semester_completed_cp\":4,\"semester_regular_weighted_cp\":9,\"semester_completed_weighted_cp\":135,\"gpa_haw\":15.00,\"gpa_de\":0.70,\"gpa_us\":4.40},{\"subjects\":[{\"abbreviation\":\"BU\",\"name\":\"Bus Systems\",\"semester\":6,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"OS\",\"name\":\"Operating Systems\",\"semester\":6,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"DP\",\"name\":\"Digital Signal Processing\",\"semester\":6,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"DC\",\"name\":\"Digital Communication Systems\",\"semester\":6,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"CJ1\",\"name\":\"Elective Project\",\"semester\":6,\"creditPoints\":5,\"weighting\":0}],\"semester_regular_cp\":29,\"semester_completed_cp\":12,\"semester_regular_weighted_cp\":48,\"semester_completed_weighted_cp\":252,\"gpa_haw\":10.50,\"gpa_de\":2.20,\"gpa_us\":2.85},{\"subjects\":[{\"abbreviation\":\"CJ2\",\"name\":\"Compulsory Project\",\"semester\":7,\"creditPoints\":5,\"weighting\":0},{\"abbreviation\":\"CM1\",\"name\":\"Elective Course 1\",\"semester\":7,\"creditPoints\":5,\"weighting\":10},{\"abbreviation\":\"CM2\",\"name\":\"Elective Course 2\",\"semester\":7,\"creditPoints\":5,\"weighting\":10},{\"abbreviation\":\"BT\",\"name\":\"Bachelor Thesis\",\"semester\":7,\"creditPoints\":15,\"weighting\":70}],\"semester_regular_cp\":30,\"semester_completed_cp\":0,\"semester_regular_weighted_cp\":90,\"semester_completed_weighted_cp\":0,\"gpa_haw\":0.00,\"gpa_de\":0.00,\"gpa_us\":0.00}]}";
        this.mockMvc.perform(get("/guest/transcript").params(gradesMap)).andDo(print()).andExpect(status().isOk())
                .andExpect(content().json(transcriptJSON));
    }
    @Test
    void calculateOverallGpa_empty() throws Exception {
        this.mockMvc.perform(get("/guest/transcript")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.gpa_haw", is(0.00)));
    }

    @Test
    void calculateOverallGpa_9112021() throws Exception {
        this.mockMvc.perform(get("/guest/transcript/gpa").params(gradesMap)).andDo(print()).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.gpa_haw", is(11.82)))
                .andExpect(jsonPath("$.gpa_de", is(1.74)));
    }

    @Test
    void calculateSemesterGpa_empty() throws Exception {
        this.mockMvc.perform(get("/guest/transcript/gpa/1")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.gpa_haw", is(0.0)))
                .andExpect(jsonPath("$.gpa_de", is(0.0)));
    }

    @Test
    void calculateSemesterGpa_9112021() throws Exception {
        this.mockMvc.perform(get("/guest/transcript/gpa/3").params(gradesMap)).andDo(print()).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.gpa_de", is(1.32)));
    }

}