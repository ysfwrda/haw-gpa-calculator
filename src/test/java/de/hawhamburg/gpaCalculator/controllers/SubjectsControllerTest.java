package de.hawhamburg.gpaCalculator.controllers;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class SubjectsControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Test
    void testGetSubjects_allsemesters() throws Exception {
        var subjectsJSON = "[{\"abbreviation\":\"MA1\",\"name\":\"Mathematics 1\",\"semester\":1,\"creditPoints\":8,\"weighting\":8},{\"abbreviation\":\"MA2\",\"name\":\"Mathematics 2\",\"semester\":2,\"creditPoints\":8,\"weighting\":8},{\"abbreviation\":\"EE1\",\"name\":\"Electrical Engineering 1\",\"semester\":1,\"creditPoints\":6,\"weighting\":6},{\"abbreviation\":\"EE2\",\"name\":\"Electrical Engineering 2\",\"semester\":2,\"creditPoints\":6,\"weighting\":6},{\"abbreviation\":\"EL1\",\"name\":\"Electronics 1\",\"semester\":2,\"creditPoints\":6,\"weighting\":6},{\"abbreviation\":\"SO1\",\"name\":\"Software Construction 1\",\"semester\":1,\"creditPoints\":7,\"weighting\":7},{\"abbreviation\":\"SO2\",\"name\":\"Software Construction 2\",\"semester\":2,\"creditPoints\":6,\"weighting\":6},{\"abbreviation\":\"GE\",\"name\":\"German\",\"semester\":1,\"creditPoints\":4,\"weighting\":0},{\"abbreviation\":\"IC\",\"name\":\"Intercultural Competence\",\"semester\":2,\"creditPoints\":3,\"weighting\":0},{\"abbreviation\":\"LSL1\",\"name\":\"Learning and Study Methods 1\",\"semester\":1,\"creditPoints\":4,\"weighting\":0},{\"abbreviation\":\"LSL2\",\"name\":\"Learning and Study Methods 2\",\"semester\":2,\"creditPoints\":2,\"weighting\":0},{\"abbreviation\":\"SS1\",\"name\":\"Signals and Systems 1\",\"semester\":3,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"SS2\",\"name\":\"Signals and Systems 2\",\"semester\":4,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"EL2\",\"name\":\"Electronics 2\",\"semester\":3,\"creditPoints\":7,\"weighting\":14},{\"abbreviation\":\"DI\",\"name\":\"Digital Circuits\",\"semester\":3,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"DS\",\"name\":\"Digital Systems\",\"semester\":4,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"MC\",\"name\":\"Microcontrollers\",\"semester\":4,\"creditPoints\":7,\"weighting\":14},{\"abbreviation\":\"AD\",\"name\":\"Algorithms and Data Structures\",\"semester\":3,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"SE\",\"name\":\"Software Engineering\",\"semester\":4,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"DB\",\"name\":\"Databases\",\"semester\":4,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"EM\",\"name\":\"Economics and Management\",\"semester\":3,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"SP\",\"name\":\"Scientific Writing\",\"semester\":5,\"creditPoints\":4,\"weighting\":9},{\"abbreviation\":\"IP\",\"name\":\"Internship\",\"semester\":5,\"creditPoints\":25,\"weighting\":0},{\"abbreviation\":\"BU\",\"name\":\"Bus Systems\",\"semester\":6,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"OS\",\"name\":\"Operating Systems\",\"semester\":6,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"DP\",\"name\":\"Digital Signal Processing\",\"semester\":6,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"DC\",\"name\":\"Digital Communication Systems\",\"semester\":6,\"creditPoints\":6,\"weighting\":12},{\"abbreviation\":\"CJ1\",\"name\":\"Elective Project\",\"semester\":6,\"creditPoints\":5,\"weighting\":0},{\"abbreviation\":\"CJ2\",\"name\":\"Compulsory Project\",\"semester\":7,\"creditPoints\":5,\"weighting\":0},{\"abbreviation\":\"CM1\",\"name\":\"Elective Course 1\",\"semester\":7,\"creditPoints\":5,\"weighting\":10},{\"abbreviation\":\"CM2\",\"name\":\"Elective Course 2\",\"semester\":7,\"creditPoints\":5,\"weighting\":10},{\"abbreviation\":\"BT\",\"name\":\"Bachelor Thesis\",\"semester\":7,\"creditPoints\":15,\"weighting\":70}]";
        this.mockMvc.perform(get("/subjects")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().json(subjectsJSON));
    }

    @Test
    void testGetSubjectAbbreviations_all() throws Exception {
        String subjectAbbreviationsJSON = "[\"MA1\",\"MA2\",\"EE1\",\"EE2\",\"EL1\",\"SO1\",\"SO2\",\"GE\",\"IC\",\"LSL1\",\"LSL2\",\"SS1\",\"SS2\",\"EL2\",\"DI\",\"DS\",\"MC\",\"AD\",\"SE\",\"DB\",\"EM\",\"SP\",\"IP\",\"BU\",\"OS\",\"DP\",\"DC\",\"CJ1\",\"CJ2\",\"CM1\",\"CM2\",\"BT\"]";
        this.mockMvc.perform(get("/subjects/abbreviations")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().json(subjectAbbreviationsJSON));
    }

    @Test
    void testGetSubjects_sem1() throws Exception {
        var semesterOneSubjectsJSON = "[{\"abbreviation\":\"MA1\",\"name\":\"Mathematics 1\",\"semester\":1,\"creditPoints\":8,\"weighting\":8},{\"abbreviation\":\"EE1\",\"name\":\"Electrical Engineering 1\",\"semester\":1,\"creditPoints\":6,\"weighting\":6},{\"abbreviation\":\"SO1\",\"name\":\"Software Construction 1\",\"semester\":1,\"creditPoints\":7,\"weighting\":7},{\"abbreviation\":\"GE\",\"name\":\"German\",\"semester\":1,\"creditPoints\":4,\"weighting\":0},{\"abbreviation\":\"LSL1\",\"name\":\"Learning and Study Methods 1\",\"semester\":1,\"creditPoints\":4,\"weighting\":0}]";
        this.mockMvc.perform(get("/subjects/1")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(semesterOneSubjectsJSON));
    }

    @Test
    void testGetSubjectAbbreviations_sem1() throws Exception {
        var semesterOneSubjectAbbreviationsJSON = "[\"MA1\",\"EE1\",\"SO1\",\"GE\",\"LSL1\"]";
        this.mockMvc.perform(get("/subjects/1/abbreviations")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(semesterOneSubjectAbbreviationsJSON));
    }


}
